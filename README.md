# Secure and provisioning a Raspberry Pi 4

Raúl Molina García, November 2019

## 1. Introduction

This is the main ansible project to secure and provisioning a Raspberry Pi 4. I have divided the process in two Ansible projects:
- Secure: This is the firt step and consist in creating a new user and disabling the default pi user. This fisrt step must be executed once.
- Provision: This is the main provisioning Ansible process and will be executed every time the repository us updated. This process will be executed in the new user account.

The Ansible scripts must be executed locally in the Raspberry due to Ansible is not officially supported in Windows.

## 2. Install a fresh raspbian operating system

Follow [this](url)https://magpi.raspberrypi.org/articles/set-up-raspberry-pi-4 instructions but install the Lite raspbian version

## 2. Generate a pair of RSA keys in your Windows system

Follow [this](https://docs.joyent.com/public-cloud/getting-started/ssh-keys/generating-an-ssh-key-manually/manually-generating-your-ssh-key-in-windows) instructions.

## 3. Link your RSA key to your gitlab account

Follow [this](https://docs.gitlab.com/ee/ssh/#adding-an-ssh-key-to-your-gitlab-account) instructions.

## 4. Copy your RSA to the raspberry

From powershell o git-bash:
```
ssh pi@raspberrypi "mkdir -p ~/.ssh"
scp $home/.ssh/id_rsa pi@raspberrypi:~/.ssh/id_rsa
ssh pi@raspberrypi "chmod 600 ~/.ssh/id_rsa"
```

## 5. Install base software

From pi account:
```
sudo apt-get update
sudo apt-get upgrade
sudo apt install ansible
sudo apt install git
```

## 6. Clone the secure repository
From pi account:
```
eval $(ssh-agent -s)
ssh-add ~/.ssh/id_rsa
git clone git@gitlab.com:elmolina/raspberrypi/securize.git
```

## 7. Configure the securice project
Edit the `securize/roles/common/vars/main.yml` and set your new username and public key

## 8. Execute the playbook
From pi account:
```
cd securize
sudo ansible-playbook local.yml
```

## 9. Lock pi user

While logged in as pi, `sudo passwd <user>` (or whatever username you created) to set a password for that user. This is not required to log in as that user, but it is required to sudo as that user. You may also choose to set a password for the pi and/or root users.

`sudo usermod --lock pi` to ensure that the default user is completely disabled.

## 10. Configure the provision project
Edit the `provision/roles/common/vars/main.yml` and set your configuration

## 11. Provisioning Raspberry
Enter with SSH to your Raspberry with the username set un your main.yml file an execute
```
cd provision
sudo ansible-playbook local.yml
```
